import kivy
kivy.require('1.9.1')
from kivy.app import App as KApp
from kivy.uix.button import Button as KButton
from kivy.properties import StringProperty
from kivy.config import Config

Config.set('graphics', 'width', '500')
Config.set('graphics', 'height', '300')
Config.set('graphics', 'resizable', 'False')
Config.set('graphics', 'position', 'custom')
Config.set('graphics', 'top', '400')
Config.set('graphics', 'left', '700')

from kivy.core.window import Window


from funct import BracketFetcher

class LayoutApp(KApp):
    eventid = StringProperty()

    def __init__(self):
        KApp.__init__(self)
        self.bf = BracketFetcher()
        

    def build(self):
        self.title = "BracketFetcher"
        self.initial_center = Window.center
        pass



LayoutApp().run()
