from urllib.error import HTTPError
import urllib.request as request
import json
import os

class BracketFetcher(object):

    def run(self, eventid, text):
        text.text = "Fetching data from smash.gg..."
        if os.name == 'nt':
            filepath="queryresult\\"
        else:
            filepath="queryresult/"
        roundnames = ['WSF', 'WF', 'GF', 'GFR', 'LQF', 'LSF', 'LF']
        doubles = [u'WSF', u'LQF', u'LSF', u'LEF']
        url = "https://api.smash.gg/phase_group/" + eventid + "?expand[]=sets&expand[]=entrants"        
        try:
            response = request.urlopen(url)
            data = json.loads(response.read().decode('utf-8'))
            if data.get('entities').get('sets') is None:
                raise Exception('ERROR: Corrupt event URL, not found') 
        except HTTPError as h:
            text.text = "ERROR: Bracket not found"
            return
        except Exception as e:
            text.text = 'ERROR: Invalid bracket ID'
            return

        text.text = "Processing data..."

        lqs = []
        rounds = []
        for match in data['entities']['sets']:
            if match['shortRoundText'] == 'LQF':
                lqs.append(match['id'])

            if match['shortRoundText'] in roundnames:
                rounds.append((match['shortRoundText'], match))

        les = [min(lqs)-1, min(lqs)-2]
        for match in data['entities']['sets']:
            if match['id'] in les:
                rounds.append((u'LEF', match))

        finalr = []
        for i in range(len(rounds)):
            ret = []
            if rounds[i][1]['entrant1Id'] == None:
                ret.append("")
            else:
                for entrant in data['entities']['entrants']:
                    if rounds[i][1]['entrant1Id'] == entrant['id']:
                        ret.append((entrant['name'], rounds[i][1]['entrant1Score']))
                        break

            if rounds[i][1]['entrant2Id'] == None:
                ret.append("")
            else:
                for entrant in data['entities']['entrants']:
                    if rounds[i][1]['entrant2Id'] == entrant['id']:
                        ret.append((entrant['name'], rounds[i][1]['entrant2Score']))
                        break
            if len(ret) > 0:
                finalr.append((rounds[i][0], ret[0], ret[1]))

        text.text = "Writing to files..."

        parity=0
        for roun in finalr:
            fp1 = None
            fp2 = None
            fs1 = None
            fs2 = None
            if roun[0] in doubles:
                fp1=open(filepath + roun[0] + str((parity%2)+1) + 'P1.txt', 'w')
                fp2=open(filepath + roun[0] + str((parity%2)+1) + 'P2.txt', 'w')
                fs1=open(filepath + roun[0] + str((parity%2)+1) + 'S1.txt', 'w')
                fs2=open(filepath + roun[0] + str((parity%2)+1) + 'S2.txt', 'w')
                parity += 1
            else:
                fp1=open(filepath + roun[0] + 'P1.txt', 'w')
                fp2=open(filepath + roun[0] + 'P2.txt', 'w')
                fs1=open(filepath + roun[0] + 'S1.txt', 'w')
                fs2=open(filepath + roun[0] + 'S2.txt', 'w')

            if roun[1] != '':
                fp1.write(roun[1][0])
                fs1.write(str(roun[1][1]))

            if roun[2] != '':
                fp2.write(roun[2][0])
                fs2.write(str(roun[2][1]))
            fp1.close()
            fp2.close()
            fs1.close()
            fs2.close()

        text.text = "Successfully fetched!"
