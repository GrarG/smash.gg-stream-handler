# Stream Handler Project

### Top 8 module:

##### Summary:

The Top 8 module allows you to easily get the Top 8 bracket from a Smash.gg tournament (players and scores) in real time and print them to files accessible by OBS or other kinds of streaming software.

##### Dependencies and installation:

The project is coded in Python 3.4, so a [Python interpreter](http://www.python.org/downloads/) is needed to run the program.

Also the [kivy](https://kivy.org/#home) is needed to run the GUI code.

##### Use
Run with `python BracketFetcher.py`.  
Input the Bracket ID of the tournament: _https://smash.gg/tournament/tournamentname/events/eventname/brackets/somenumber/_**bracketID**(this).  
Click on *Fetch*.  
If the bracket exists, and is public, the files will be generated in the *queryresult* folder.

### About

Author: Antonio Oliva Hernández
Current version: 1.0
This project is developed under a GNU GPLv3 license.
